@def title = "MyLab"

# Homework

![Alt Text](/assets/HW.jpg)


## MyLab assignments

* [Chapter 2.5 HW](https://mlm.pearson.com/northamerica/) is posted in MyLab: Due on Thursday, Oct.7th. 
* [Chapter 2.3 and 2.4 HW](https://mlm.pearson.com/northamerica/) is posted in MyLab: Due on Thursday, Oct.7th. 
* [Chapter 2.1 and 2.2 HW](https://mlm.pearson.com/northamerica/) is posted in MyLab: Due on Thursday, Oct.7th. 
* [Chapter 1.7 HW](https://mlm.pearson.com/northamerica/) is posted in MyLab: Due on Monday, Oct.4th. 
* [Chapter 1.6 HW](https://mlm.pearson.com/northamerica/) is posted in MyLab: Due on Wednesday, Sep.22nd. 
* [Chapter 1.5 HW](https://mlm.pearson.com/northamerica/) is posted in MyLab: Due on Thursday, Sep.16th. 
* [Chapter 1.4 HW](https://mlm.pearson.com/northamerica/) is posted in MyLab: Due on Tuesday, Sep. 14th.
* [Chapter 1.3 HW](https://mlm.pearson.com/northamerica/) is posted. Due on Friday, Sep. 10th.
* [Chapter 1.2 HW](https://mlm.pearson.com/northamerica/) is posted. Due on Friday, Sep. 8th.
* [Chapter 1.1 HW](https://mlm.pearson.com/northamerica/) is posted. Due on Friday, Sep. 6th. 
* [HW 2](https://mlm.pearson.com/northamerica/) is posted. Due on Friday, Sep. 3rd. 
* [HW 1](https://mlm.pearson.com/northamerica/) is posted. Due on Tuesday, Aug. 31st. 




## Useful MyLab Info

* Head over to the [MyLab](https://mlm.pearson.com/northamerica/) 






