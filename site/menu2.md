@def title = "Class Notes"

# Class Notes

![Alt Text](/assets/notes3.jpg)

## Final Exam Review 

* Monday, Nov 29th
    - [Part 2](https://tinyurl.com/35eyeac2)
    - Missed the lecture, No worries [click here](https://texastech.zoom.us/rec/share/xbH8eZLhbNCEd2_BVNT44-UXHGf6mQJbFvhc9micOQlZWruHsLu2tiDd2VVSksZS.DFgdfnV5ah0C2zuV?startTime=1638230710000)

* Monday, Nov 22nd
    - [Part 1 - chapters 4 and 5.1 problems](https://tinyurl.com/2p9b85u7)
    - Missed the lecture, No worries [click here](https://youtu.be/vKIJ7tRAxx8)



## Week 15

* Wednesday, Dec. 1st
    - [Chapter  8.7 - please watch the video to see the note](https://tinyurl.com/ess8e3ym)
    - [Chapter  5.5 - please watch the video to see the note](https://tinyurl.com/ess8e3ym)
    - Missed the lecture, No worries [click here](https://tinyurl.com/yckrm82p)

* Monday, Nov. 29th

    - [Chapter 5.4](https://tinyurl.com/wrmur6rk)
    - Missed the lecture, No worries [click here](https://tinyurl.com/2yzmeeyp)    

## Week 14

* Monday, Nov. 22nd

    - [Chapters 5.1 (part 2) and  5.2](https://tinyurl.com/2p9xxh7e)
    - Missed the lecture, No worries [click here](https://youtu.be/OVsj-dxekyI)


## Week 13

* Friday, Nov. 19th
    - [Chapter 5.1 (part 1)](https://tinyurl.com/vja5sk)
    - [Chapter 4.5 (part 2)](https://tinyurl.com/99bnm3us)
    - Missed the lecture, No worries [click here](https://youtu.be/p5yWQWD9I5g)

* Wednesday, Nov. 17th
    - [Chapter  4.5 (part 1)](https://tinyurl.com/ess8e3ym)
    - [Chapter  4.4 (part 2)](https://tinyurl.com/3jh4ekk8)
    - Missed the lecture, No worries [click here](https://youtu.be/fjvJdzHiMDM)

* Monday, Nov. 15th

    - [Chapter  4.4 (part 1)](https://tinyurl.com/wrmur6rk)
    - [Chapter  4.3 (part 2)](https://tinyurl.com/ct7nmxyr)
    - Missed the lecture, No worries [click here](https://youtu.be/FGtmBjSAMaA)



## Week 12

* Friday, Nov. 12th
    - [Chapter  4.2 and 4.3 (part 1)](https://tinyurl.com/ytaduhdf)
    - [Chapter  4.1](https://tinyurl.com/2cdjc837)
    - Missed the lecture, No worries [click here](https://youtu.be/prI2_nhtG3M)

* Wednesday, Nov. 10th
    - [Exam 3 review](https://tinyurl.com/2tu2rryd) 
    - Missed the review, No worries [click here](https://youtu.be/3lW8ArREifs) 

    - [Chapter  3.6](https://tinyurl.com/538xbjp5)
    - Missed the lecture, No worries [click here](https://youtu.be/LbCHvSXKwhQ)

* Monday, Nov. 8th

    - [Chapter  3.5](https://tinyurl.com/3z3k3k6p)
    - Missed the lecture, No worries [click here](https://youtu.be/JBqq2GYRTv8)


## Week 11

* Friday, Nov. 5th

    - [Chapter  3.4 (part 2)](https://tinyurl.com/52f8ajf7)
    - Missed the lecture, No worries [click here](https://youtu.be/uN5vaU1T_1Q)

* Wednesday, Nov. 3rd

    - [Chapter  3.4 (part 1)](https://tinyurl.com/jy4j33uw)
    - [Chapter  3.3 (part 2)](https://tinyurl.com/exmj92j8)
    - Missed the lecture, No worries [click here](https://youtu.be/M7v6YgOng1I)


* Monday, Nov. 1st

    - [Chapter  3.3 (part 1)](https://tinyurl.com/eh2ckjp8)
    - Missed the lecture, No worries [click here](https://youtu.be/TEJd4BX5_Fk)

 

## Week 10

* Friday, Oct. 29th

    - [Chapter  3.2 (part 2)](https://tinyurl.com/sk8m7c5m)
    - Missed the lecture, No worries [click here](https://youtu.be/73O0XMEfpKk)

* Wednesday, Oct. 27th

    - [Chapter  3.2 (part 1)](https://tinyurl.com/4dyzpvaw)
    - Missed the lecture, No worries [click here](https://youtu.be/3WSV1Re01II)

* Monday, Oct. 25th
    - [Chapter 3.1 (part 2)](https://tinyurl.com/7kjwh99f)
    - Missed the lecture, No worries [click here](https://texastech.zoom.us/rec/share/PCyN99UUvPid4WeRFuPUYjDteNQS2e8mxbPWMeVlgz81bn4ds4_zwAdW7NQmTc7F.y6cVLPQnuEahaEfg?startTime=1635174037000)



## Week 9

* Friday, Oct. 22nd

    - Missed the review, No worries [click here](https://texastech.zoom.us/rec/share/4ZQO1MEV503G7LghE0NCz9Qn2tUwa95RAOgslJHTkbjsBIZJ5TlbQGnxHvr13q9N.hcvlQeof0ox982WP?startTime=1634842856000)


* Wednesday, Oct. 20th

    - [Chapter  3.1 (part 1)](https://tinyurl.com/f5s9zzb8)
    - [Chapter  2.7 (part 2)]( )
    - Missed the lecture, No worries [click here](https://youtu.be/UJ_rNrSIjzg)

* Monday, Oct. 18th
    - [Chapter  2.6 (part 2) and 2.7 (part 1)](https://tinyurl.com/fyf6t249)
    - Missed the lecture, No worries [click here](https://youtu.be/jiDGgUQmWQU)


## Week 8

* Friday, Oct. 15th

    - [Chapter  2.6 (part 1)](https://tinyurl.com/f2ptvuu2)
    - Missed the lecture, No worries [click here](https://youtu.be/tHMTA7Da-p8)

* Wednesday, Oct. 13th

    - [Chapter  2.5 (part 2)](https://tinyurl.com/7k7jmk5t)
    - Missed the lecture, No worries [click here](https://youtu.be/6OU2stU0nd4)

* Monday, Oct. 11th
    - [Chapter  2.5 (part 1)](https://tinyurl.com/fyf6t249)
    - Missed the lecture, No worries [click here](https://youtu.be/Exub1X1vT1w)



## Week 7

* Friday, Oct. 8th

    - [Chapter 2.3 and 2.4 (part 2)](https://tinyurl.com/vxmzk6pk)
    - Missed the lecture, No worries [click here](https://youtu.be/lhkSFM_W6rs)

* Wednesday, Oct. 6th

    - [Chapter 2.3 and 2.4 (part 1)](https://tinyurl.com/tz2pj5je)
    - Missed the lecture, No worries [click here](https://youtu.be/7PJQR7uQU-w)

* Monday, Oct. 4th
    - [Chapter  2.1 and 2.2 (part 3)](https://tinyurl.com/58zb8mcc)
    - Missed the lecture, No worries [click here](https://youtu.be/QWxh10qdmMs)

## Week 6 

* Friday, Oct. 1st
    - [Chapter 2.1 and 2.2 (part 2)](https://tinyurl.com/58zb8mcc)
    - Missed the lecture, No worries [click here](https://youtu.be/4e-kBmbsAYo)

* Wednesday, Sep. 29th

    - [Chapter 2.1 and 2.2 (part 1)](https://tinyurl.com/58zb8mcc)
    - [Chapter 1.7 (part 2)](https://tinyurl.com/4uvhb7kc)
    - Missed the lecture, No worries [click here](https://youtu.be/9wqhvwunQRc)

* Monday, Sep. 27th
    - [Chapter 1.7 (part 1)](https://tinyurl.com/h852u3hb)
    - Missed the lecture, No worries [click here](https://youtu.be/bbMapuIW9LA)

## Week 5 

* Friday, Sep. 24th
    - Exam day 
     
* Wednesday, Sep. 22nd
    - [Few review problems](https://tinyurl.com/yddp2esf)
    - Missed the lecture, No worries [click here](https://youtu.be/eotkc2TD44Q)

* Monday, Sep. 20th
    - [Chapter 1.6 (part 2)](https://tinyurl.com/jspmxpad)
    - Missed the lecture, No worries [click here](https://youtu.be/Lkw9jWcXr8w)


## Week 4 

* Friday, Sep. 17th
    - [Chapter 1.6 (part 1)](https://tinyurl.com/jspmxpad)
    - Missed the lecture, No worries [click here](https://youtu.be/tZz-BLrCiI8)

* Wednesday, Sep. 15th
    - [Chapter 1.5 (part 2)](https://tinyurl.com/yrhurv7y)
    - Missed the lecture, No worries [click here](https://youtu.be/zsoauNhuMhk)

* Monday, Sep. 13th
    - [Chapter 1.5 (part 1)](https://tinyurl.com/2w53ejtx)
    - Missed the lecture, No worries [click here](https://youtu.be/mSx6Nl1pw0c)

    

## Week 3 

* Friday, Sep. 10th
    - [Chapter 1.4 (part 2)](https://tinyurl.com/4z6vvcaa)
    - Missed the lecture, No worries [click here](https://youtu.be/S6N5E8-1AOk)

* Wednesday, Sep. 8th
    - [Chapter 1.4 (part 1)](https://tinyurl.com/4z6vvcaa)
    - [Chapter 1.3 (part 2)](https://tinyurl.com/4snk2k55)
    - Missed the lecture, No worries [click here](https://youtu.be/hLVzZk4JA1w)


## Week 2 

* Friday, Sep. 3rd
    - [Chapter 1.3 (part 1)](https://tinyurl.com/4snk2k55)
    - [Chapter 1.2 (part 2)](https://tinyurl.com/amv9t2ps)
    - Missed the lecture, No worries [click here](https://youtu.be/mbpig9hK2WU)

* Wednesday, Sep. 1st
    - [Chapter 1.2 (part 1)](https://tinyurl.com/amv9t2ps)
    - [Chapter 1.1 (part 2)](https://tinyurl.com/fsd43e3n)
    - Missed the lecture, No worries [click here](https://youtu.be/4fbBFp_PLfQ)

* Monday, Aug. 30th
    - [Chapter 1.1 (part 1)](https://tinyurl.com/2uf3m7zu)
    - [Review (part 3)](https://tinyurl.com/6zmevc8p)
    - Missed the lecture, No worries [click here](https://youtu.be/jWWTh9wY_BA)

## Week 1 

* Friday, Aug. 27th
    - [Review (part 2)](https://tinyurl.com/6zmevc8p)

* Wednesay, Aug. 25th
    - [Review (part 1)](https://tinyurl.com/6zmevc8p)

* Monday, Aug. 23rd
    - [Welcome slides!](https://tinyurl.com/s5wfwvc2)



