@def title = "Welcome"
@def tags = ["syntax", "code"]

# Welcome to the World of College Algebra! 

 
<!-- \tableofcontents  you can use \toc as well -->

**_MATH 1320: Most important math class you will ever take!_**

![Alt Text](/assets/toronto.gif)

---

* [Exam 2 instructions lives here!](https://tinyurl.com/kar6u4aw)

* [Exam 1 instructions lives here!](https://tinyurl.com/pr3dd8cj)

---

[Anonymous Lecture Feedback](https://forms.gle/PwAcJftswqb4TCU47)

---

* Head over to the [MyLab](https://mlm.pearson.com/northamerica/)
* Head over to the [Blackboard](https://ttu.blackboard.com/)

---

## Things to be known:


----

Skeleton notes [lives here!](https://tinyurl.com/3uje6up9)

Course syllabus [lives here!](https://tinyurl.com/35bw8p93)


* **Lecture:** MWF: 10:00-10:50am, Holden 077 
    - Live class session via ZOOM: [Click here!!!](https://zoom.us/j/8065430626)  
* **Instructor:** Bhagya Athukorallage, PhD
* **Office Hours:**  MWF: 11:00am - 11:50am, Th: 1:00pm - 1:50pm, & F: 9:00-9:50am or by appointments
    - [Click here to enter my ZOOM office](https://zoom.us/j/8065430626)
* **Office:** MATH 253
* **E-mail:** bhagya.athukorala@ttu.edu







